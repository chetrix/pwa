importScripts('/src/js/idb.js');
importScripts('/src/js/utility.js');

var CACHE_STATIC_NAME = 'static-v1';
var CACHE_DYNAMIC_NAME = 'dynamic-v2';
var STATIC_FILES = [
    '/',
    '/index.html',
    '/offline.html',
    '/src/js/app.js',
    '/src/js/utility.js',
    '/src/js/feed.js',
    '/src/js/idb.js',
    '/src/js/material.min.js',
    '/src/css/app.css',
    '/src/css/feed.css',
    '/src/images/main-image.jpg',
    'https://fonts.googleapis.com/css?family=Roboto:400,700',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css'
]

// const dbPromise = idb.open('posts-store', 1, function (db) {
//     if (!db.objectStoreNames.contains('posts')) {
//         db.createObjectStore('posts', { keyPath: 'id' });
//     }
// });
// function trimCache(cacheName, maxItems) {
//     caches.open(cacheName)
//       .then((cache=>{
//           return cache.keys()
//            .then((keys)=>{
//             if(keys.length> maxItems){
//                 cache.delete(keys[0])
//                   .then(trimCache(cacheName, maxItems));
//             }
//         })
//       })
//       )
// }
self.addEventListener('install', function (event) {
    console.log('installing service worker', event)
    event.waitUntil(
        caches.open(CACHE_STATIC_NAME)
            .then((cache) => {
                console.log('precaching app shell');
                cache.addAll(STATIC_FILES)
            })
    )                          //waits until a given operation which has to return a promise
    // and caches.open() return a promise . i.e it wont finish installation event before caches.open()
})

self.addEventListener('activate', function (event) {
    console.log('activating service worker', event);
    event.waitUntil(
        caches.keys()
            .then((keyList) => {
                return Promise.all(keyList.map((key) => {
                    if (key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
                        console.log('[Service worker] removing old cache', key);
                        return caches.delete(key);
                    }
                }))
            })
    )
})

// self.addEventListener('fetch', function (event) {
//     console.log('fetch')
//     event.respondWith(
//         caches.match(event.request)
//             .then((response) => {
//                 if (response) {
//                     return response;
//                 } else {
//                     return fetch(event.request)
//                         .then((res) => {
//                             return caches.open(CACHE_DYNAMIC_NAME)
//                                 .then((cache) => {
//                                     cache.put(event.request.url, res.clone());
//                                     //the response if we store it here is basically consumed which means its empty
//                                     // You can only consume or use them once and storing them here in the cache uses the response
//                                     //So we use res.clone(). It creates a clone. So that we are not consuming the response and returning the original response
//                                     //Because this is how response works!!!
//                                     return res;
//                                 })
//                         })
//                         .catch((err) => {
//                             return caches.open(CACHE_STATIC_NAME)
//                                 .then((cache) => {
//                                     return cache.match('/offline.html');
//                                 })
//                         })
//                 }
//             })
//     )
// })

function isInArray(string, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === string) {
            return true;
        }
    }
    return false;
}

//Cache first and the network

self.addEventListener('fetch', function (event) {
    var url = 'https://pwapwa-9901f-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json';

    if (event.request.url.indexOf(url) > -1) {
        // event.respondWith(
        //     caches.open(CACHE_DYNAMIC_NAME)
        //         .then((cache) => {
        //             return fetch(event.request)
        //                 .then((res) => {
        //                     // trimCache(CACHE_DYNAMIC_NAME, 3);
        //                     cache.put(event.request, res.clone());
        //                     return res;
        //                 })
        //         })
        // )
        event.respondWith(fetch(event.request)
            .then((res) => {
                var clonedRes = res.clone();
                clearAllData('posts')
                  .then(()=>{
                    return clonedRes.json()
                  })
                  .then((data) => {
                    for (var key in data) {
                        // dbPromise
                        //   .then((db)=>{
                        //       var tx = db.transaction('posts', 'readwrite');
                        //       var store = tx.objectStore('posts') //open the store
                        //       store.put(data[key]);
                        //       return tx.complete;
                        //   })
                        writeData('posts', data[key])
                    }
                })
                return res;
            })
        )
    } else if (isInArray(event.request.url, STATIC_FILES)) {
        // else if(new RegExp('\\b'+ STATIC_FILES.join('\\b|\\b') + '\\b').test(event.request.url)) {
        event.respondWith(
            caches.match(event.request)
        )
    } else {
        event.respondWith(
            caches.match(event.request)
                .then((response) => {
                    if (response) {
                        return response;
                    } else {
                        return fetch(event.request)
                            .then((res) => {
                                return caches.open(CACHE_DYNAMIC_NAME)
                                    .then((cache) => {
                                        // trimCache(CACHE_DYNAMIC_NAME, 3);
                                        cache.put(event.request.url, res.clone());
                                        return res;
                                    })
                            })
                            .catch((err) => {
                                return caches.open(CACHE_STATIC_NAME)
                                    .then((cache) => {
                                        // if(event.request.url.indexOf('/help')>-1)
                                        if (event.request.headers.get('accept').includes('text/html')) {
                                            return cache.match('/offline.html');
                                        }
                                    })
                            })
                    }
                })
        )
    }
})

// Network first and cache callback

// self.addEventListener('fetch', function(event){
//     event.respondWith(
//         fetch(event.request)
//           .then((res)=>{
//             return caches.open(CACHE_DYNAMIC_NAME)
//                 .then((cache)=>{
//                     cache.put(event.request.url, res.clone()); 
//                     return res;
//             })
//           })
//           .catch((err)=>{
//             return caches.match(event.request)
//           })
//     )
// })

// Pre-Cache only

// self.addEventListener('fetch', function(event){
//     event.respondWith(
//         caches.match(event.request)
//     )
// })

//Network only

// self.addEventListener('fetch', function(event){
//     event.respondWith(
//         fetch(event.request)
//     )
// })

//sync before posting image

// self.addEventListener('sync', (event)=>{
//     console.log('[Service Worker] Background syncing', event)
//     if(event.tag==='sync-new-posts'){
//         console.log('[Service worker] Syncing new posts');
//         event.waitUntil(
//             readAllData('sync-posts')
//               .then((data)=>{
//                   for(var dt of data){
//                       console.log(dt, data)
//                     fetch('https://pwapwa-9901f-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json', {   //this url has to be replaced by functions url
//                         method: "POST",
//                         headers: {
//                           'Content-Type': 'application/json',
//                           'Accept': 'applicatin/json'
//                         },
//                         body: JSON.stringify({
//                           id: dt.id,
//                           title: dt.title,
//                           location: dt.location,
//                           image: 'https://firebasestorage.googleapis.com/v0/b/pwapwa-9901f.appspot.com/o/sf-boat.jpg?alt=media&token=4951ef19-2d46-4e71-8389-9fc6be337fac'
//                         }),
//                       })
//                       .then((res)=>{
//                         console.log('Sent data', res);
//                         console.log(dt, data)
//                         if(res.ok){
//                             // res.json()
//                             //   .then((resData)=>{
//                             //     deleteItemFromData('sync-posts', resData.id)
//                             //   })
//                             deleteItemFromData('sync-posts', dt.id)
//                         }
//                       })
//                       .catch((err)=>{
//                           console.log('Error while sending data', err);
//                       })
//                   }
//               })
//         )
//     }
// })


//Storing image in a server

self.addEventListener('sync', (event)=>{
    console.log('[Service Worker] Background syncing', event)
    if(event.tag==='sync-new-posts'){
        console.log('[Service worker] Syncing new posts');
        event.waitUntil(
            readAllData('sync-posts')
              .then((data)=>{
                  for(var dt of data){
                    var postData = new FormData();
                    postData.append('id', dt.id);
                    postData.append('title', dt.title);
                    postData.append('location', dt.location);
                    postData.append('file', dt.picture, dt.id+'.png')
                    postData.append('rawLocationLat', dt.rawLocation.lat);
                    postData.append('rawLocationLng', dt.rawLocationLng.lng);
                    fetch('https://pwapwa-9901f-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json', {   //this url has to be replaced by functions url
                        method: "POST",
                        body: postData
                      })
                      .then((res)=>{
                        console.log('Sent data', res);
                        console.log(dt, data)
                        if(res.ok){
                            // res.json()
                            //   .then((resData)=>{
                            //     deleteItemFromData('sync-posts', resData.id)
                            //   })
                            deleteItemFromData('sync-posts', dt.id)
                        }
                      })
                      .catch((err)=>{
                          console.log('Error while sending data', err);
                      })
                  }
              })
        )
    }
})

self.addEventListener('notificationclick', (event)=>{
    console.log(self);
    var notification = event.notification;
    var action = event.action;

    if(action==='confirm'){
        console.log('Confirm was chosen');
        notification.close();
    }else{
        console.log(action);
        event.waitUntil(
            clients.matchAll() //clients refers to all opened windows or browser tasks related to this service worker. matchAll() to access them all
              .then((clis)=>{
                  console.log(clis)
                  var client = clis.find((c)=>{
                      return c.visibilityState='visible';
                  });

                //   if(client !==undefined) {
                //       client.navigate('notification.data.url');
                //       client.focus();
                //   } else{
                //       clients.openWindow('notification.data.url');
                //   }
                  notification.close();
              })                           
        )
    }
})

self.addEventListener('notificationclose', (event)=>{
    console.log('Notification was closed', event);
})

self.addEventListener('push', (event)=>{
    console.log('Push Notification received', event);

    var data = { title: "New!", content: "Something new happened!", openUrl: '/' }//fallback
    if(event.data){
        data=JSON.parse(event.data.text())
    }

    var options = {
        body: data.content,
        icon: "/src/images/icons/app-icon-96x96.png",   
        badge: "/src/images/icons/app-icon-96x96.png",
        data: {
            url: data.OpenUrl
        }
      };

      event.waitUntil(
          self.registration.showNotification(data.title, options) //the active service worker itself can't show the notification,it's there to listen to events it's running in the background.
                                                                  //That's why we have to get access to the registration of the service worker(that is the part running in the browser so to say).so it's the part which connects the service worker to the browser.
      )
})