var deferredPrompt;
var enableNotificationsButtons = document.querySelectorAll(
  ".enable-notifications"
);

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/sw.js")
    .then(() => console.log("service worker registered!"))
    .catch((err) => console.log(err));
}

window.addEventListener("beforeinstallprompt", function (event) {
  console.log("before install prompt fired");
  event.preventDefault();
  deferredPrompt = event;
  return false;
});

function displayConfirmNotification() {
  // var options = {
  //     body: 'You successfully subscribed to our Notifications service!'
  // };
  // new Notification('Successfully Subsribed!');
  if ("serviceWorker" in navigator) {
    var options = {
      body: "You successfully subscribed to our notifications [SW]",
      icon: "/src/images/icons/app-icon-96x96.png",
      image: "/src/images/sf-boat.jpg",
      dir: "ltr",
      lang: "en-US",
      vibrate: [100, 50, 200],
      badge: "/src/images/icons/app-icon-96x96.png",
      tag: "confirm-notification",
      renotify: true,
      actions: [
        {
          action: "confirm",
          title: "Okay",
          icons: "/src/images/icons/app-icon-96x96.png",
        },
        {
          action: "cancel",
          title: "Cancel",
          icons: "/src/images/icons/app-icon-96x96.png",
        },
      ],
    };
    navigator.serviceWorker.ready.then((swreg) => {
      console.log(swreg)
      swreg.showNotification("Successfully subscribed!", options);
    });
  }
}

function configurePushSub() {
  if (!("serviceWorker" in navigator)) {
    return;
  }
  var reg;

  navigator.serviceWorker.ready
    .then((swreg) => {
        console.log(swreg)
        reg=swreg;
      return swreg.pushManager.getSubscription();
    })
    .then((sub) => {
      if (sub === null) {
          var vapidPublicKey = 'BEPYU4V9k12Wc7n8vCp5HLyVCSfrqZfFdeV25bv3CF2OHy9LRVHlgeY0b85ap5usNsDFdm39FQ7uHeBbR8B-32k'
          var convertedVapidPublicKey = urlBase64ToUint8Array(vapidPublicKey);
          return reg.pushManager.subscribe({
            userVisibleOnly: true ,
            applicationServerKey: convertedVapidPublicKey  // push notifications sent through our service  are only visible to this user
        })                                                 //checking if subscription exist for the current browser
                                                           //in the same device. Different browser will have different subs in the same device
      } else {
      }
    })
    .then((newSub)=>{
        return fetch('https://pwapwa-9901f-default-rtdb.asia-southeast1.firebasedatabase.app/subscriptions.json',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({newSub})
        })
    })
    .then((res)=>{
        if(res.ok) {
            displayConfirmNotification();
        }
    })
    .catch((err)=>{
        console.log(err);
    });
}

function askForNotificationPermission() {
  Notification.requestPermission((result) => {
    console.log("User Choice", result);
    if (result !== "granted") {
      console.log("No Permission granted!");
    } else {
      configurePushSub();
      // displayConfirmNotification();
    }
  });
}

if ("Notification" in window && "serviceWorker" in navigator) {
  for (var i = 0; i < enableNotificationsButtons.length; i++) {
    enableNotificationsButtons[i].style.display = "inline-block";
    enableNotificationsButtons[i].addEventListener(
      "click",
      askForNotificationPermission
    );
  }
}
