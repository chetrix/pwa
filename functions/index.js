//formidable for accessing formData and @google-cloud/storage for fire base storage as firebasestorage uses google cloud storage behind the scene.
//we can't use that admin package we already use to access the database,
//that just doesn't give us access to storage.

const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cors = require("cors")({ origin: true });
var webpush = require("web-push");
var formidable = require('formidable');
var fs = require('fs');
const UUID = require('uuid-v4');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
var serviceAccount = require("./xxx.json");

const gcconfig = {
  projectId: 'pwagram-99adf',
  keyFilename: 'pwagram-fb-key.json'
}

var gcs = require('@google-cloud/storage')(gcconfig);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL:
    "https://pwapwa-9901f-default-rtdb.asia-southeast1.firebasedatabase.app",
});
exports.storePostData = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    var uuid = UUID();
    var formData = new formidable.IncomingForm() // this is a helper which allows us to automatically fetch it from the incoming request
    formData.parse(request, (err, fields, files)=>{   //fields are just everything but files 
      fs.rename(files.file.path, '/tmp', files.file.name);
      var bucket = gcs.bucket('pwapwa-9901f.appspot.com');
      bucket.upload('/tmp/'+ files.file.name,{
        uploadType: 'media',
        metadata : {
          metadata: {
            contentType: files.file.type,
            firebaseStorageDownloadTokens:  uuid
          }
        }
      }, (err, file)=>{
        if(!err){
          admin
      .database()
      .ref("posts")
      .push({
        id: fields.id,                     //formidable gives access to fields
        title: fields.title,
        location: fields.location,
        rawLocation:{
          lat: fields.rawLocationLat,
          lng: fields.rawLocationLng
        },
        image: 'https://firebasestorage.googleapis.com/v0/b/'+ bucket.name+'/o/'+encodeURI(file.name)+'?alt=media&token=' + uuid,
      })
      .then(() => {
        webpush.setVapidDetails(
          "mailto: metal.maniacggmu@gmail.com",
          "BEPYU4V9k12Wc7n8vCp5HLyVCSfrqZfFdeV25bv3CF2OHy9LRVHlgeY0b85ap5usNsDFdm39FQ7uHeBbR8B-32k",
          "XLARWXSjGAbwqWhwKgr5t78A5UYqSadwU2Z-U1-_ofw"
        );
        admin.database().ref("subscriptions").once("value");
      })
      .then((subscriptions) => {
        subscriptions.forEach((sub) => {
          var pushConfig = {
            endpoint: sub.val().endpoint,
            keys: {
              auth: sub.val().keys.auth,
              p256dh: sub.val().keys.p256dh,
            },
          };
          webpush
            .sendNotification(
              pushConfig,
              JSON.stringify({ title: "New Post", content: "New Post added!" , openUrl: '/help'})
            ) //this returns promise . but instead of then block, we listenm catch block
            .catch((err) => {
              console.log(err);
            });
        });
        response
          .status(201)
          .json({ message: "Data Stored", id: fields.id });
      })
      .catch((err) => {
        response.status(500).json({ error: err });
      });
        }else{
          console.log(err);
        }
      });
    })
  });
});
